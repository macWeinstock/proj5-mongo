"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    #Change start time to an arrow object
    brevet_start_time = arrow.get(brevet_start_time)
    brevet_dist_km = int(brevet_dist_km)
    #Redundant check for nosetests
    if not check_valid_distances(control_dist_km, brevet_dist_km):
      return False

    #Checks for when control and brevet are identical (end of race) or control is slightly over total brevet
    if control_dist_km == 0:
      return brevet_start_time.isoformat()
    if ((control_dist_km >= 200) and (control_dist_km <= 240)) and (brevet_dist_km == 200):
      return brevet_start_time.shift(hours=+5, minutes=+53).isoformat()
    if ((control_dist_km >= 300) and (control_dist_km <= 360)) and (brevet_dist_km == 300):
      return brevet_start_time.shift(hours=+9).isoformat()
    if ((control_dist_km >= 400) and (control_dist_km <= 480)) and (brevet_dist_km == 400):
      return brevet_start_time.shift(hours=+12, minutes=+8).isoformat()
    if ((control_dist_km >= 600) and (control_dist_km <= 720)) and (brevet_dist_km == 600):
      return brevet_start_time.shift(hours=+18, minutes=+48).isoformat()
    if ((control_dist_km >= 1000) and (control_dist_km <= 1200)) and (brevet_dist_km == 1000):
      return brevet_start_time.shift(hours=+33, minutes=+5).isoformat()

    if (control_dist_km < 200):
      #Calculate appropriate time based on the control distance
      time = control_dist_km / 34
      #Convert time to something arrow can work with
      hours, minutes, seconds = time_converter(time)
      #Calculate the control
      control_open = brevet_start_time.shift(hours=+hours, minutes=+minutes, seconds=+seconds)
      #Small workaround to zero out seconds and miliseconds for nosetests
      control_open = control_open.replace(second=0, microsecond=0)
      #return the open time in ISO 8601 format
      return control_open.isoformat()
    if (control_dist_km < 400):
      #Perform the same calculation but with a different max speed for the first 200 km
      control_dist_km -= 200
      #Calculate the time of the remainder after accounting for the speedf of the first 200 km
      #and add the result
      time = (control_dist_km / 32) + (200 / 34)
      #Convert time and calculate the control open in ISO 8601 format and return
      hours, minutes, seconds = time_converter(time)
      control_open = brevet_start_time.shift(hours=+hours, minutes=+minutes, seconds=+seconds)
      control_open = control_open.replace(second=0, microsecond=0)
      return control_open.isoformat()
    if (control_dist_km < 600):
      #Follow similar steps as the last elif including the 200-400 km range and speed
      control_dist_km -= 400
      time = (control_dist_km / 30) + (200 / 34) + (200/32)
      hours, minutes, seconds = time_converter(time)
      control_open = brevet_start_time.shift(hours=+hours, minutes=+minutes, seconds=+seconds)
      control_open = control_open.replace(second=0, microsecond=0)
      return control_open.isoformat()
    if (control_dist_km < 1000):
      #Follow similar steps as the last elif including the 400-600 km range and speed
      control_dist_km -= 600
      time = (control_dist_km / 28) + (200 / 34) + (200/32) + (200/30)
      hours, minutes, seconds = time_converter(time)
      control_open = brevet_start_time.shift(hours=+hours, minutes=+minutes, seconds=+seconds) 
      control_open = control_open.replace(second=0, microsecond=0)
      return control_open.isoformat()
    
    return False


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    brevet_start_time = arrow.get(brevet_start_time)

    brevet_dist_km = int(brevet_dist_km)
    #Redundant check for nosetests
    if not check_valid_distances(control_dist_km, brevet_dist_km):
      return False

    #Checks for when control and brevet are identical (end of race) or control is slightly over total brevet
    if control_dist_km == 0:
      control_close = brevet_start_time.shift(hours=+1)
      return control_close.isoformat()
    if ((control_dist_km >= 200) and (control_dist_km <= 240)) and (brevet_dist_km == 200):
      return brevet_start_time.shift(hours=+13, minutes=+30).isoformat()
    if ((control_dist_km >= 300) and (control_dist_km <= 360)) and (brevet_dist_km == 300):
      return brevet_start_time.shift(hours=+20).isoformat()
    if ((control_dist_km >= 400) and (control_dist_km <= 480)) and (brevet_dist_km == 400):
      return brevet_start_time.shift(hours=+27).isoformat()
    if ((control_dist_km >= 600) and (control_dist_km <= 720)) and (brevet_dist_km == 600):
      return brevet_start_time.shift(hours=+40).isoformat()
    if ((control_dist_km >= 1000) and (control_dist_km <= 1200)) and (brevet_dist_km == 1000):
      return brevet_start_time.shift(hours=+75).isoformat()
    
    if (control_dist_km < 600):
      #Similar calculation as in the open_time function except with a minimum speed (15) as
      #opposed to maximum speed
      time = control_dist_km / 15
      hours, minutes, seconds = time_converter(time)
      control_close = brevet_start_time.shift(hours=+hours, minutes=+minutes, seconds=+seconds)
      control_close = control_close.replace(second=0, microsecond=0)
      return control_close.isoformat()
    if (control_dist_km) < 1000 and (brevet_dist_km == 1000):
      #Account for distances over 600 which have a different minimum speed (11.428)
      #using a similar calculation as in the elif statements in the open_times function
      control_dist_km -= 600
      time = (control_dist_km / 11.428) + (600 / 15)
      hours, minutes, seconds = time_converter(time)
      control_close = brevet_start_time.shift(hours=+hours, minutes=+minutes, seconds=+seconds)
      control_close = control_close.replace(second=0, microsecond=0)
      return control_close.isoformat()

    return False


def time_converter(time):
  #Helper function to calculate and format time
  hours = int(time)
  minutes = (time*60) % 60
  seconds = (time*3600) % 60
  return hours, minutes, int(seconds)


def check_valid_distances(control_dist_km, brevet_dist_km):
  #Check for control distances less than 0, greater than 100, or greater than 25% of
  #the final brevet distance
  brevet_dist_km = int(brevet_dist_km)
  if (control_dist_km < 0) or (control_dist_km > 1200) or (control_dist_km > (brevet_dist_km + (brevet_dist_km * .25))):
    return False
  
  return True













