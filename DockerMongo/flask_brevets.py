"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import os
import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from pymongo import MongoClient

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

###
#Mongo Client
###

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    db.tododb.drop()
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    begin_time = request.args.get('begin_time')
    begin_date = request.args.get('begin_date')
    brevet_dist_km = request.args.get("brevet_dist_km")

    #Check for valid distances in the acp_times file and return an "invalid"
    #JSON object if returns false. Otherwise continue.
    if not acp_times.check_valid_distances(km, brevet_dist_km):
        result = {"invalid": km}
        return flask.jsonify(result=result)

    #Got the following line from piazza @87
    date_time_string = begin_date + ' ' + begin_time + ':00'

    open_time = acp_times.open_time(km, brevet_dist_km, date_time_string)
    close_time = acp_times.close_time(km, brevet_dist_km, date_time_string)

    result = {"open": open_time, "close": close_time}

    return flask.jsonify(result=result)


@app.route('/_submit', methods=['GET','POST'])
def submit():
    result = {}
    if request.method == "POST":
       result = {
            "open" : request.form['open'],
            "close" : request.form['close']
       }
    else:
        return "Error, method was not 'POST'"

    val = list(result.values())
    app.logger.debug(val)

    #Awful way to error check, but it works while maintaining regular functionality
    if val[0] == "[]":
        result = { "open": "ERROR: no entries", "close": "ERROR: no entries" }
        db.tododb.insert_one(result)
        return "ERROR: no entries"

    #Awful way to error check, but it works while maintaining regular functionality
    if "null" in val[0]:
        result = { "open": "ERROR: null entry found", "close": "ERROR: null entry found" }
        db.tododb.insert_one(result)
        return "ERROR: null entry found"

    app.logger.debug(result)

    db.tododb.insert_one(result)

    return "ok"
    

@app.route('/_display', methods=['POST'])
def display():
    _items = db.tododb.find()
    items = [item for item in _items]

    return flask.render_template('display.html', items=items)

#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
