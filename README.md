# Project 5: Brevet time calculator with Ajax and MongoDB

Reimplement the RUSA ACP controle time calculator with flask and ajax using a database to store and display results.

Credits to Michal Young for the initial version of this code.

# Author: Mac Weinstock
# Contact: mweinsto@uoregon.edu

## ACP controle times

The algorithm for calculating controle times is described here (https://rusa.org/pages/acp-brevet-control-times-calculator). Additional background information is given here (https://rusa.org/pages/rulesForRiders). 

We are essentially replacing the calculator here (https://rusa.org/octime_acp.html) storing results in a database. I've added submit and display buttons to store open and close times in a MongoDB instance and display to find the results and display in a new tab.


## Some test cases:

(1) Display error string when nothing is entered by the user

(2) Display error string when there is a null entry, meaning the user skipped a control

(3) If method sent to server is not a POST, return an error string to console
